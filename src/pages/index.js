import React from 'react'
import Link from 'gatsby-link'

const IndexPage = () => (
  <div>
    <h1>Hi Yusuf Habibillah</h1>
    <p>Welcome to your new Gatsby site.</p>
    <p>Now go build something great. Do you feel it?</p>
    <Link to="/page-2/">Show me other page</Link>
  </div>
)

export default IndexPage
